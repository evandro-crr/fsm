#pragma once
#include <map>
#include <regex>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <utility>
#include <stdexcept>
#include <unordered_map>

class Automata {
 public:
  Automata(const char path[]);
  void add_state(const std::string &name, bool final_state);
  void set_init(const std::string &name);
  void add_transition(const std::string &actual_states,
                     const char condition,
                     const std::string &next_state);
  bool compute(const char input[]) const;

 private:
  void epsilon_transition(std::vector<unsigned> &actual_state) const;

  void tokenize(const char path[]);
  void tokenize_state(const std::string &line);
  void tokenize_transition(const std::string &line);

  std::vector<bool> states;  
  std::unordered_map<std::string ,unsigned> names;
  unsigned init;
  std::map<std::pair<unsigned, char>, std::vector<unsigned>> transition;
};
