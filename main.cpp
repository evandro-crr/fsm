#include <iostream>
#include "automata.h"

void prompt(const Automata &automata) {

  while (true) {
    std::string input;
    std::cout << "Input: ";
    std::cin >> input;
    
    std::cout << (automata.compute(input.c_str())? ":)\n" : ":(\n") << std::endl;
  }

}

void input_file(const Automata &automata, const char path[]) {

  std::ifstream file{path};
  std::string line, input;

  while (std::getline(file, line)) input += line;

  input = std::regex_replace(input ,std::regex("\\s+"), "");

  std::stringstream input_stream{input};

  while (std::getline(input_stream, line, ';')) {
    std::cout << "Input: " << line << std::endl;
    std::cout << (automata.compute(line.c_str())? ":)\n" : ":(\n") << std::endl;
  }

}

int main(const int argc, const char* argv[]) {

  Automata automata{argv[1]};
 
  switch (argc) {
      case 2:
      prompt(automata);
      break;
    case 3:
      input_file(automata, argv[2]);  
      break;
    default:
      std::cerr << argv[0] << " automata_file.m [input_file.i]\n";
  }

  return 0;
}

