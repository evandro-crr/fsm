#include "automata.h"

void Automata::add_state(const std::string &name, bool final_state) {
  if (name == "") throw std::logic_error{"undefined state"};
  states.push_back(final_state);
  names[name] = states.size() -1;
}

void Automata::set_init(const std::string &name) {
  try {
    init = names.at(name);
  } catch (const std::out_of_range) {
    throw std::out_of_range{"undefined initial state"};
  }
}

void Automata::add_transition(const std::string &actual_state,
                             const char condition,
                             const std::string &next_state) {
  try {
    transition[std::make_pair(names.at(actual_state), condition)]
              .push_back(names.at(next_state));
  } catch (const std::out_of_range) {
    throw std::out_of_range{"undefined transition"};
  }
}

void Automata::epsilon_transition(std::vector<unsigned> &actual_states) const {
  for (auto i = 0; i < actual_states.size(); i++) {
    try {
      auto result = transition.at(std::make_pair(actual_states[i], 0));
      actual_states.insert(actual_states.end(), result.begin(), result.end());
    } catch (const std::out_of_range) {}
  }  
}

bool Automata::compute(const char input[]) const {
  std::vector<unsigned> actual_states{init}; 

  for (auto i = 0; input[i]; i++) {
    
    epsilon_transition(actual_states);

    std::vector<unsigned> next_states;

    for (auto state : actual_states) {
      try {
        auto result = transition.at(std::make_pair(state, input[i]));
        next_states.insert(next_states.end(), result.begin(), result.end());
        } catch (const std::out_of_range) {}
    }
    
    if (next_states.empty()) return false;
    
    actual_states = next_states;
  }

  epsilon_transition(actual_states);

  for (auto state : actual_states) {
    if (states[state]) return true;
  }

  return false;
}

void Automata::tokenize_state(const std::string &line) {
  std::stringstream line_stream{line};
  std::string token;

  bool have_init{false};
  bool have_final{false};

  while (std::getline(line_stream, token, ',')) {
    bool final_state{false};
    
    if (token[token.size()-1] == '*') {
      have_final = true;
      final_state = true;
      token = token.substr(0, token.size()-1);
    }

    if (token.substr(0, 2) == "->") {
      if (have_init == true) throw std::logic_error{"More than one initial state"};

      have_init = true;
      token = token.substr(2, token.size()-2);

      add_state(token, final_state);
      set_init(token);

    } else {
      add_state(token, final_state);
    }
  }

  if (not have_init) throw std::logic_error{"without final state"};
  if (not have_final) throw std::logic_error{"without initial state"};
}

void Automata::tokenize_transition(const std::string &line) {
  std::stringstream line_stream{line};
  std::string token;

  while (std::getline(line_stream, token, ',')) {
    std::stringstream token_stream{token};
    
    std::string actual_states;
    std::string condition;
    std::string next_state;
    
    std::getline(token_stream, actual_states, ' ');
    std::getline(token_stream, condition, ' ');
    if (condition.size() > 1)
      throw std::logic_error{"transition condition must just have one character"};
    std::getline(token_stream, next_state, ' ');

    add_transition(actual_states, condition[0] == '_'? 0 : condition[0], next_state);
  }
}

void Automata::tokenize(const char path[]) {
  std::ifstream file{path};
  std::string line;

  std::getline(file, line, ';');
  tokenize_state(std::regex_replace(line, std::regex("\\s+"), ""));

  std::getline(file, line, ';');
  line = std::regex_replace(line, std::regex("\\n"), "");
  line = std::regex_replace(line, std::regex("  "), " ");
  line = std::regex_replace(line, std::regex("  "), " ");
  tokenize_transition(line);
}

Automata::Automata(const char path[]) {
  tokenize(path);
}
